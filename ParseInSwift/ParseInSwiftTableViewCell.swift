//
//  ParseInSwiftTableViewCell.swift
//  ParseInSwift
//
//  Created by jmoralez on 1/20/16.
//  Copyright © 2016 Bathroom Gaming LLC. All rights reserved.
//

import UIKit
import ParseUI

class ParseInSwiftTableViewCell: PFTableViewCell {

    @IBOutlet weak var catImageView:UIImageView?
    @IBOutlet weak var catNameLabel:UILabel?
    @IBOutlet weak var catVotesLabel:UILabel?
    @IBOutlet weak var catCreditLabel:UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
